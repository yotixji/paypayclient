import {
    PEER_REVIEWS_LIST
} from '../action/review/review.actionType';

export default function( state ={}, action) {
    switch(action.type) {
        case PEER_REVIEWS_LIST:
            return {
                state,
                peerReviews:action.payload,
                isLoading: false
            }
        default:
            return state;
    }
}