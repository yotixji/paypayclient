import {
    combineReducers
  } from 'redux';
  
import authReducer from './auth.reducer';
import reviewReducer from './review.reducer';
  
export default combineReducers({
    auth: authReducer,
    reviews: reviewReducer
});
  