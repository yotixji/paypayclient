import {
    LOGIN_START,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    LOGOUT_SUCCESS,
    UPDATE_USER,
  } from '../action/auth/auth.actionType';
  
  export default function(state = {}, action) {
    switch (action.type) {
      case LOGIN_START:
        return {
          ...state,
          isProcessing: true,
        };
      case LOGIN_SUCCESS: {
        return {
          ...state,
          user: action.payload.user,
          accessToken: action.payload.accessToken,
          isProcessing: false,
        };
      }
      case LOGIN_ERROR:
        return {
          ...state,
          isProcessing: false,
          error: action.payload,
          user: null,
          accessToken: null,
        };
      case UPDATE_USER: {
        return {
          ...state,
          user: action.payload,
        };
      }
      case LOGOUT_SUCCESS:
        return {
          ...state,
          user: null,
          accessToken: null,
        };
      default:
        return state;
    }
  }
  