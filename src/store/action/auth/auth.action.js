import {
setLocalStorage,
removeLocalStorage,
getLocalStorage
} from '../../../utils/web-storage';
import axiosService from '../../../inits/axios';
import history from '../../../inits/history';
import {
LOGIN_START,
LOGIN_SUCCESS,
LOGIN_ERROR,
LOGOUT_SUCCESS,
UPDATE_USER,
} from './auth.actionType';

export const login = (
    userCredential
    ) => async (dispatch, getState) => {
    try {
        dispatch({
        type: LOGIN_START
        });
        const tokenInfo = 
        await axiosService.post('/login', userCredential);
        await dispatch(getCurrentUser(tokenInfo.data.token.auth_token,tokenInfo.data.user_id));
        window.location.href = '/';
        history.push('/');
    } catch (error) {
        dispatch({
        type: LOGIN_ERROR,
        payload: Object.values(error)
        });
    }
};

export const getCurrentUser = (accessToken, user_id) => async (dispatch, getState) => {
    setLocalStorage('accessToken', accessToken);
    setLocalStorage('user_id',user_id);
    try {
        const userInfo = await axiosService.get('/profile/'+user_id);
        dispatch({
        type: LOGIN_SUCCESS,
        payload: {
            user: userInfo.data,
            accessToken
        }
        });
        return Promise.resolve(userInfo.data);
    } catch (error) {
        dispatch({
        type: LOGIN_ERROR,
        payload: error
        });
        return Promise.reject(error);
    }
};

export const logout = () => async (dispatch, getState) => {
    try {
        dispatch(removeUser());
        history.push('/login');
    } catch (error) {
        dispatch(removeUser());
        history.push('/login');
        return Promise.reject(error);
    }
};

export const removeUser = () => async (dispatch, getState) => {
    removeLocalStorage('accessToken');
    dispatch({
        type: LOGOUT_SUCCESS
    });
};

export const editProfile = user => async (dispatch, getState) => {
    try {
        const userInfo = await axiosService.post(`/users/profile`, user);
        dispatch({
        type: UPDATE_USER,
        payload: userInfo.data
        });
    } catch (error) {
        return Promise.reject(error);
    }
};
