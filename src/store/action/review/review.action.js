import axiosService from '../../../inits/axios';
import history from '../../../inits/history';
import {
    PEER_REVIEWS_LIST,
} from './review.actionType';

export const getPeerReviews = () => async dispatch => {
  try {
    const reviewsInfo = await axiosService.get('/reviews');
    dispatch({
      type: PEER_REVIEWS_LIST,
      payload: reviewsInfo.data
    })
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getReviewById = reviewId => async dispatch => {
    try {
      const reviewInfo = await axiosService.get(`/reviews/${reviewId}`);
      return Promise.resolve(reviewInfo.data);
    } catch (error) {
      return Promise.reject(error);
    }
  }

export const submitReview = (review) => async dispatch => {
  console.log(review);
  try {
    await axiosService.patch(`/reviews/${review.id}`,review);
    dispatch({
      type: 'SUBMIT_REVIEW'
    })
    history.push('/');
  }
  catch(error) {
    return Promise.reject(error);
  }
}