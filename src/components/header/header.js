import React from 'react';
import './header.css';
import { connect } from 'react-redux';
import { logout } from '../../store/action/auth/auth.action';

class Header extends React.PureComponent {

    render() {
        return (
            <nav className="navbar navbar-light bg-light">
                <a href="/" className="navbar-brand">
                    PayPay
                </a>
                <ul className="nav-menu">
                    { this.props.user ?
                    <li className="link"><span className="text-primary" onClick={this.props.onLogoutClick}>Logout</span></li>
                    : null
                    }
                </ul>
            </nav>
        )
    }
}

const mapStateToProps = state => {
    return {
      user: state.auth.user
    };
  };
const mapDispatchToProps = {
    onLogoutClick: logout,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);