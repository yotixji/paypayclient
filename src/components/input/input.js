import React from 'react';
import './input.css';

export default function Input(props) {
    return (
        <div className="form-group">
          <label>{props.label}</label>
          <input type={props.type} onChange={props.onChange} 
            value={props.value} name={props.name} 
            className="form-control" required={props.required} />
      </div>
    )
} 