import React from 'react';

export const ReviewInput = props => {
    const value = parseInt(props.value);
    return (
        <>
        <div className="row">
        <div className="col-md-6"><b>{props.title}</b>{props.subtitle ? ` : ${props.subtitle}`: null} </div>
        <div className="col-md-6 row">
            
            <div className="col-md-3 text-center">
                <input type="radio" name={props.name} value={4} 
                            checked={value === 4} 
                            onChange={props.onChange} required/>
            </div>
            <div className="col-md-3 text-center">  
                <input type="radio" name={props.name} value={3} 
                            checked={value === 3} 
                            onChange={props.onChange} />
            </div>
            <div className="col-md-3 text-center">
                <input type="radio" name={props.name} value={2} 
                                checked={value === 2} 
                                onChange={props.onChange} />
            </div>
            <div className="col-md-3 text-center">
                <input type="radio" name={props.name} value={1} 
                            checked={value === 1} 
                            onChange={props.onChange} />
            </div>
        </div>
        </div>
        <div className="row mt-2">
        <div className="form-group col-md-12">
            <textarea value={props.commentText} onChange={props.onCommentChange}  
                className="form-control"
                placeholder={'Specific example/comment?'} rows="3">

                </textarea>
        </div>
        </div>
        </>
    );
}