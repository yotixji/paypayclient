import React from 'react';
import {Route,Redirect} from 'react-router-dom';
import { getLocalStorage } from '../../utils/web-storage';

const accessToken = getLocalStorage('accessToken');

const PrivateRoute=({component:Component,authenticated,...rest})=>(
    <Route
    {...rest}
    render={props=>
        accessToken?(
        <Component {...props}/>
        ):(
        <Redirect
            to={{
                pathname:'/login',
                state:{from:props.location}
            }}
            />
        )
        }
            />
)



export default (PrivateRoute);