import React from 'react';
import { connect } from 'react-redux';
import { getPeerReviews } from '../../store/action/review/review.action';
import ReviewsList from './ReviewsList';

class WithDataReviewList extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state={
            reviews:[],
            isLoading:true
        }
    }

    componentDidMount(){
        this.loadReviews()
    }

    loadReviews = async () => {
        await this.props.getPeerReviews();
        this.setState({
            isLoading:false,
            reviews: this.props.reviews
        })
    }

    render() {
        return(
            <div className="container mt-5">
                <h3 className="text-primary">Pending Reviews List</h3>
                {this.state.isLoading ? <div>Loading data....</div> :
                    <ReviewsList
                        emptyMessage={'You have not been assigned any reviews yet !'}
                        reviews={this.state.reviews}/>
                    }
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
      reviews: state.reviews.peerReviews
    };
  };

const mapDispatchToProps = {
    getPeerReviews,
};

export default connect(mapStateToProps,mapDispatchToProps)(WithDataReviewList);