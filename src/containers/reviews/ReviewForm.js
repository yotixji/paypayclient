import React from 'react';
import './reviews.css';
import { ReviewInput } from '../../components/input/reviewInput';
import { connect } from 'react-redux';
import { submitReview, getReviewById} from '../../store/action/review/review.action';

class ReviewForm extends React.PureComponent {

    constructor(props){
        super(props);

        this.state ={
                quality:'',
                qualityText:'',
                persist:'',
                persistText:''
        }
    }

    componentDidMount(){
        this.loadReviews()
    }

    handleSubmit = event => {
        event.preventDefault();
        const review = this.state;
        // Remove Unecessary keys
        review.completed = 1
        delete review['isLoading'];
        this.props.submitReview(this.state);
    }

    loadReviews = async () => {
        const review = await this.props.getReviewById(this.props.match.params.id);
        // Initialize Review
        review.quality = review.attitude = review.commitment = review.productivity = review.knowledge -= 1
        this.setState({
            ...review
        })
    }

    render() {
        return (
            <div className="review-form container mt-5">
                <div className="row">
                    <div className="col-md-6">
                        <h2>Performance Review</h2>
                        <p className="mt-4">The purpose of the performance review is to: Develop better communications
                            between the employer and supervisor, imporve the quality of work, increase
                            productivity, and Promote employee development.
                        </p>
                    </div>
                    <div className="employee-info col-md-6"></div>
                </div>
                <form className="mt-4" onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col-md-6">General Factors</div>
                        <div className="col-md-6 row">

                        <div className="col-md-3 bold text-center">Outstanding</div>
                        <div className="col-md-3 text-center">Exceeds Expectations</div>
                        <div className="col-md-3 text-center">Meets Expectations</div>
                        <div className="col-md-3 text-center">Improvement Needed</div>
                        </div>
                    </div>
                    <ReviewInput
                    title={'Quality'}
                    subtitle={'An employees work'}
                    name={'quality'}
                    value={this.state.quality}
                    onChange={e=>this.setState({quality:e.target.value})}
                    commentText={this.state.qualityText}
                    onCommentChange={e=>this.setState({qualityText:e.target.value})}
                    />
                    <ReviewInput
                    title={'Productivity/Independance/Reliability'}
                    subtitle={'An employees work'}
                    name={'productivity'}
                    value={this.state.productivity}
                    onChange={e=>this.setState({productivity:e.target.value})}
                    commentText={this.state.productivityText}
                    onCommentChange={e=>this.setState({productivityText:e.target.value})}
                    />
                    <ReviewInput
                    title={'Job Knowledge'}
                    subtitle={'Demonstrates and understands the work instructions'}
                    name={'knowledge'}
                    value={this.state.knowledge}
                    onChange={e=>this.setState({knowledge:e.target.value})}
                    commentText={this.state.knowledgeText}
                    onCommentChange={e=>this.setState({knowledgeText:e.target.value})}
                    />
                    <ReviewInput
                    title={'Cooperation / Commitment'}
                    name={'commitment'}
                    value={this.state.commitment}
                    onChange={e=>this.setState({commitment:e.target.value})}
                    commentText={this.state.commitmentText}
                    onCommentChange={e=>this.setState({commitmentText:e.target.value})}
                    />
                    <ReviewInput
                    title={'attitude'}
                    name={'attitude'}
                    value={this.state.attitude}
                    onChange={e=>this.setState({attitude:e.target.value})}
                    commentText={this.state.attitudeText}
                    onCommentChange={e=>this.setState({attitudeText:e.target.value})}
                    />
                    <div className="text-center">
                        <input type="submit" className="btn btn-primary" value="Submit"/>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
      reviews: state.reviews.peerReviews
    };
  };

const mapDispatchToProps = {
    getReviewById,
    submitReview
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewForm);
