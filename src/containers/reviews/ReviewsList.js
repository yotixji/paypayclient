import React from 'react';

function ReviewList(props) {
    return(
        <table className="table table-sm">
                <thead>
                    <tr>
                    <th scope="col">Employee #</th>
                    <th scope="col">Name</th>
                    <th scope="col">Department</th>
                    <th scope="col">Review</th>
                    </tr>
                </thead>
                <tbody>
                    { props.reviews.length ? 
                    props.reviews.map(review=>{
                    return ( 
                            <tr key={review.id}>
                                <th scope="row">1</th>
                                <td>{review.employee.name}</td>
                                <td>{review.employee.department}</td>
                                <td><a href={`/review/${review.id}`}>Start</a></td>
                            </tr>
                            );
                    }) :
                    <tr><td colSpan="4" className="text-center">{props.emptyMessage}</td></tr> }
                </tbody>
            </table>
    )
}

export default ReviewList;