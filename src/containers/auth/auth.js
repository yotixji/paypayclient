import React from 'react';
import Input from '../../components/input/input';
import './auth.css';
import { login } from '../../store/action/auth/auth.action';//'../../store/actions/auth/auth.action';
import { connect } from 'react-redux';


class AuthContainer extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            email: "",
            password: ""
        }
    }

    handleSubmit = event => {
        event.preventDefault();
        this.props.onLoginClick(this.state);
    }

    render() {
        return (
            <div className="container col-md-4 mt-4">
                <h2 className="text-center">Login</h2>
                <form onSubmit={this.handleSubmit}>
                    <Input
                        type="text"
                        name="email"
                        label="Email Address"
                        required
                        value={this.state.email}
                        onChange={e=>this.setState({email:e.target.value})}/>
                    <Input 
                        label="Password"
                        type="password"
                        placeholder="**********"
                        name="password"
                        value={this.state.password} 
                        required
                        onChange={e=>this.setState({password:e.target.value})}/>
                    <span className="text-danger">{this.props.error?this.props.error.join(', '):null}</span>
                    <input className="btn-block btn btn-primary" type="submit" value="Submit"/>
                    <a href="/">Can't login ? Contact Support</a>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
      isLoading: state.auth.isLoading,
      error: state.auth.error
    };
  };
const mapDispatchToProps = {
    onLoginClick: login,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthContainer);