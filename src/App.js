import React from 'react';
import Header from './components/header/header';
import {
  Router,
  Route,
  Switch
} from "react-router-dom"; 
import { axiosInterceptor } from './inits/axios';
import ReviewForm from './containers/reviews/ReviewForm';
import AuthContainer from './containers/auth/auth';
import PrivateRoute from './components/private/privateRoute';
import { getLocalStorage } from './utils/web-storage';
import { Provider } from 'react-redux';
import store from './store';
import { getCurrentUser } from './store/action/auth/auth.action';
import history from './inits/history';
import withDataReviewList from './containers/reviews/withDataReviewList';

axiosInterceptor(store);

const NoMatch = ({ location }) => (
  <div>
    <h3>
      No match for <code>{location.pathname}</code>
    </h3>
  </div>
);

class App extends React.Component {
  state = {
    loading: true,
  };
  UNSAFE_componentWillMount() {
    this.authenticateUser();
  }

  authenticateUser = async () => {
    const accessToken = getLocalStorage('accessToken');
    const user_id = getLocalStorage('user_id');
    if (accessToken) {
      try {
        const getUser = store.dispatch(getCurrentUser(accessToken, user_id));
        await Promise.all([getUser]);
      } catch (error) {
        console.log('User not logged in!!');
      } finally {
        this.setState({
          loading: false,
        });
      }
    }
  };
  render() {
    return (
      <Provider store={store}>
      <Router history={history} basename="/">
        <div className="App">
          <Header/>
        </div>
        <Switch>
          <PrivateRoute exact path="/" component={ withDataReviewList }/>
          <Route exact path="/login" component={ AuthContainer }/>
          <PrivateRoute path="/review/:id" component={ ReviewForm }/>
          <Route component={NoMatch} />
        </Switch>
      </Router>
    </Provider>
      );
  } 
}

export default App;
